package main

import (
	"encoding/json"
	"fmt"
)

type WikiPage struct {
	Title string
	Url   string
}

type WikipediaResponse struct {
	BatchComplete string `json:"batchcomplete"`
	Query         struct {
		Pages map[string]struct {
			PageID int    `json:"pageid"`
			Ns     int    `json:"ns"`
			Title  string `json:"title"`
		} `json:"pages"`
	} `json:"query"`
}

func getRandomWikipediaPages(count int) (pages []WikiPage, err error) {
	url := fmt.Sprintf("https://en.wikipedia.org/w/api.php?format=json&action=query&generator=random&grnnamespace=0&grnlimit=%d", count)
	body, err := getCachedUrl(url, "wikipedia-randoms", "2006-01-02-15")
	if err != nil {
		return nil, fmt.Errorf("error fetching Wikipedia pages: %v", err)
	}

	var wikiResponse WikipediaResponse
	err = json.Unmarshal(body, &wikiResponse)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling Wikipedia JSON: %v", err)
	}

	pages = []WikiPage{}
	for pageID := range wikiResponse.Query.Pages {
		title := wikiResponse.Query.Pages[pageID].Title
		url := fmt.Sprintf("http://en.m.wikipedia.org/?curid=%s", pageID)
		pages = append(pages, WikiPage{Title: title, Url: url})
	}

	return pages, nil
}
