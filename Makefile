all: build test run

build:
	go build .

test:
	go test -v .

run:
	go run .
