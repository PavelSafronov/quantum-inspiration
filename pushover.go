package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
)

const (
	pushoverTokenEnvVar     = "PUSHOVER_QUANTUM_INSPIRATION_TOKEN"
	pushoverUserTokenEnvVar = "PUSHOVER_USER_TOKEN"
	maxMessageLength        = 1024
	// this project runs daily, so this is the max TTL for now
	// if this project is run more frequently, then this value should be changed
	ttlDays = 2

	// automatically calculated, do not change
	ttlSeconds = ttlDays * 24 * 60 * 60
)

func CombineUpToMaxMessageLength(prefix string, moreInfo string, messages []string) string {
	var combinedMessage = prefix
	for _, message := range messages {
		newMessage := combinedMessage + message
		if len(newMessage)+len(moreInfo) > maxMessageLength {
			combinedMessage += moreInfo
			break
		}
		combinedMessage = newMessage
	}
	return combinedMessage
}

func SendPushoverMessage(message string) error {
	if len(message) > maxMessageLength {
		return fmt.Errorf("message is too long: %d > %d", len(message), maxMessageLength)
	}
	postUrl := "https://api.pushover.net/1/messages.json"

	pushoverToken := os.Getenv(pushoverTokenEnvVar)
	pushoverUser := os.Getenv(pushoverUserTokenEnvVar)
	if pushoverUser == "" || pushoverToken == "" {
		return fmt.Errorf("missing environment variable %s or %s", pushoverUserTokenEnvVar, pushoverTokenEnvVar)
	}

	response, err := http.PostForm(postUrl, url.Values{
		"token":   {pushoverToken},
		"user":    {pushoverUser},
		"message": {message},
		"html":    {"1"},
		"ttl":     {fmt.Sprintf("%d", ttlSeconds)},
	})

	if err != nil {
		return err
	}
	defer response.Body.Close()

	isNot200 := response.StatusCode != 200
	if isNot200 {
		return fmt.Errorf("pushover returned status code %d: %s", response.StatusCode, response.Status)
	}
	return nil
}
