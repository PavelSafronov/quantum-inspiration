package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
)

const (
	qrngApiKeyEnvVar = "ANU_QRNG_KEY"
)

type apiResponse struct {
	Success bool    `json:"success"`
	Type    string  `json:"type"`
	Length  string  `json:"length"`
	Data    []uint8 `json:"data"`
}

func GetRandomNumbers(count int) ([]uint8, error) {
	resp, err := getRandomFromQrng(count)
	if err != nil {
		return nil, err
	}
	return resp.Data, nil
}

func getRandomFromQrng(count int) (apiResponse, error) {
	API_URL := "https://api.quantumnumbers.anu.edu.au"
	API_KEY, keyFound := os.LookupEnv(qrngApiKeyEnvVar)
	if !keyFound {
		fmt.Printf("Error: API key not found in environment variable %s", qrngApiKeyEnvVar)
		return apiResponse{}, fmt.Errorf("API key not found")
	}

	client := &http.Client{}
	req, err := http.NewRequest("GET", fmt.Sprintf("%s?length=%d&type=uint8", API_URL, count), nil)
	if err != nil {
		fmt.Println("Error creating request:", err)
		return apiResponse{}, err
	}
	req.Header.Set("x-api-key", API_KEY)

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("Error executing request:", err)
		return apiResponse{}, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading response:", err)
		return apiResponse{}, err
	}

	var response apiResponse
	err = json.Unmarshal(body, &response)
	if err != nil {
		fmt.Println("Error unmarshalling JSON:", err)
		return apiResponse{}, err
	}

	return response, nil
}
