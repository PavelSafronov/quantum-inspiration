# Find Stone Beer

This project is a Go application that once a day pings gets 256 random wiki pages, then picks one of those using a Quantum Random Number Generator, and then sends that article to my phone using Pushover.

## Tech used

- golang
- [ANU QRNG API](https://quantumnumbers.anu.edu.au/) for quantum random numbers
- [Wikipedia API](https://en.wikipedia.org/w/api.php?format=json&action=query&generator=random&grnnamespace=0&grnlimit=256) for getting random pages
- Pushover is used for delivering notifications to my phone
- GitLab CICD is used to perform a scheduled run of this app
