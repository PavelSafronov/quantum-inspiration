package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

func getCachedUrl(
	url string,
	id string,
	cacheFileTimeFormat string,
) ([]byte, error) {
	return getCachedUrlFunc(url, id, cacheFileTimeFormat, func(bytes []byte) ([]byte, error) {
		return bytes, nil
	})
}
func getCachedUrlFunc(
	url string,
	id string,
	cacheFileTimeFormat string,
	byteProcessor func([]byte) ([]byte, error),
) ([]byte, error) {
	now := time.Now()
	nowFormatted := now.Format(cacheFileTimeFormat)
	cacheFile := fmt.Sprintf("cache/%s-%s.txt", id, nowFormatted)
	dir := filepath.Dir(cacheFile)
	_, err := os.Stat(dir)
	dirExists := err == nil || !os.IsNotExist(err)
	if !dirExists {
		err = os.Mkdir(dir, 0755)
		if err != nil {
			return nil, err
		}
	}

	_, err = os.Stat(cacheFile)
	fileExists := err == nil || !os.IsNotExist(err)
	if fileExists {
		f, err := os.Open(cacheFile)
		if err != nil {
			return nil, err
		}
		defer f.Close()
		bytes, err := io.ReadAll(f)
		if err != nil {
			return nil, err
		}
		return bytes, nil
	} else {
		resp, err := http.Get(url)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()

		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		body, err = byteProcessor(body)
		if err != nil {
			return nil, err
		}

		f, err := os.Create(cacheFile)
		if err != nil {
			return nil, err
		}
		defer f.Close()
		_, err = f.Write(body)
		if err != nil {
			return nil, err
		}

		return body, nil
	}
}
