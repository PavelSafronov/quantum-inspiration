package main

import (
	"encoding/json"
	"fmt"
	"os"
	"time"
)

type Results struct {
	CreationDate string
	RandomNumber uint8
	Pages        []WikiPage
}

func main() {
	random256Pages, err := getRandomWikipediaPages(256)
	if err != nil {
		panic(fmt.Errorf("error getting random Wikipedia pages: %v", err))
	}

	randomNumbers, err := GetRandomNumbers(1)
	if err != nil {
		panic(fmt.Errorf("error getting random numbers: %v", err))
	}

	random := randomNumbers[0]

	result := Results{
		Pages:        random256Pages,
		RandomNumber: random,
		CreationDate: time.Now().Format("2006-01-02"),
	}
	resultJson, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		panic(fmt.Errorf("error marshalling results: %v", err))
	}
	completeJson := fmt.Sprintf("var results = %s;", resultJson)
	outPath := "./public/results.js"
	err = os.WriteFile(outPath, []byte(completeJson), 0644)
	if err != nil {
		panic(fmt.Errorf("error writing to %s: %v", outPath, err))
	}

	page := random256Pages[random]
	title := page.Title
	url := page.Url
	message := fmt.Sprintf("Quantum inspiration for the day: <a href='%s'>%s</a>\n\n<a href='https://pavelsafronov.gitlab.io/quantum-inspiration/'>More...</a>", url, title)
	err = SendPushoverMessage(message)
	if err != nil {
		panic(err)
	}
}
